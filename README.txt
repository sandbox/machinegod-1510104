// $Id$

mgEveCache - Eve online API response cache
----------------------------------------------------



No configuration is necessary. 

The module will clear old cache entries with the normal cron tasks.



Known incompatibilities
-----------------------

No incompatibilities with other modules are known to this date.


Maintainers
-----------
mgEveCache is written by Michael Lehman - Drakos Wraith in game.



